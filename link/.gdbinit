# load gdb-dashboard
source ~/.dotfiles/caches/gdb-dashboard/.gdbinit

# display locals in the stack
dashboard stack -style locals True
# only display the source and the stack
dashboard -layout source stack
# no jellybeans palette :(
dashboard -style syntax_highlighting 'monokai'
