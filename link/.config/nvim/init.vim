""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Limeth's Neovim configuration
"-------------------------------------------------------------------------------
" A powerful configuration for development in various languages including Java,
" Vala, Crystal, HTML and Stylus.
"
" There must be the following symlinks for Neovim to work correctly:
" * `~/.vim` -> `~/.config/nvim`
" * `~/.vimrc` -> `~/.config/nvim/init.vim`
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use a different shell than Fish
if &shell =~# 'fish$'
  set shell=bash
endif

" Choose a colorscheme
"colorscheme molokai
colorscheme ../plugged/jellybeans.vim/colors/jellybeans
let g:jellybeans_use_term_italics = 1

" Enable python plugins in neovim
if has('nvim')
  runtime! plugin/python_setup.vim
endif

" Plugins (commands: Plug)
source ~/.vim/plug.vim
" Values (commands: set, let)
source ~/.vim/value.vim
" Functions (command: function)
source ~/.vim/function.vim
" Mappings (commands: various map commands)
source ~/.vim/map.vim
" coc config
source ~/.vim/coc.vim
" Execute automatically (commands: augroup, autocmd)
source ~/.vim/auto.vim

" Display whitespace
call ToggleInvisibles()
