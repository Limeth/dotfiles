""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VALUES
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Vimspector (Debugging)
let g:vimspector_base_dir=expand('$HOME/.vim/plugged/vimspector')
let g:vimspector_enable_mappings = 'HUMAN'
let g:vimspector_install_gadgets = ['debugpy', 'vscode-cpptools', 'CodeLLDB']

" Rainbow
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle

" Racket
let g:syntastic_enable_racket_racket_checker = 1

" Julia Automatic LaTeX symbol substitution
let g:latex_to_unicode_auto = 1

" Language Server Protocol settings
let g:LanguageClient_autoStart = 1
set formatexpr=LanguageClient_textDocument_rangeFormatting_sync()
let java_language_server_binary = system('find ~/workspace/java/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/plugins/org.eclipse.equinox.launcher_*.jar | sort -r | sed -e "q" | perl -pe "chomp if eof"')
" clangd installation:
" Add PPAs according to https://apt.llvm.org/
" `sudo apt-get install clang-tools-8`
" `sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-8 100`
    " \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
let g:LanguageClient_serverCommands = {
    \ 'rust': ['rust-analyzer'],
    \ 'java': ['java', '-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044', '-Declipse.application=org.eclipse.jdt.ls.core.id1', '-Dosgi.bundles.defaultStartLevel=4', '-Declipse.product=org.eclipse.jdt.ls.core.product', '-Dlog.level=ALL', '-noverify', '-Xmx1G', '-jar', java_language_server_binary, '-configuration', '~/workspace/java/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/config_linux', '-data', '~/workspace/java'],
    \ 'c': ['clangd-8'],
    \ 'cpp': ['clangd-8'],
\ }

"" Snippets and autocompletion
"let g:neosnippet#enable_complete_done = 1
"" Plugin key-mappings.
"" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)

"" SuperTab like snippets behavior.
"" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
""imap <expr><TAB>
"" \ pumvisible() ? "\<C-n>" :
"" \ neosnippet#expandable_or_jumpable() ?
"" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
"smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" echodoc
set cmdheight=2
let g:echodoc#enable_at_startup = 1

" " clangd: Always draw the signcolumn.
" set signcolumn=yes

" ESLint for javascript
let g:syntastic_javascript_checkers = ['eslint']

" If there's a `meson.build` file, use meson for linting.
autocmd FileType c call ConsiderMesonForLinting()
function ConsiderMesonForLinting()
    if filereadable('meson.build')
        " Initialize meson if the `build` directory is missing
        if !isdirectory('build')
            silent !meson build
        endif

        let g:syntastic_c_checkers = ['meson']
    endif
endfunction

" Vala settings
"let vala_ignore_valadoc = 1  " Disable valadoc syntax highlight
let vala_comment_strings = 1  " Enable comment strings
let vala_space_errors = 1  " Highlight space errors
"let vala_no_trail_space_error = 1  " Disable trailing space errors
let vala_no_tab_space_error = 1  " Disable space-tab-space errors
"let vala_minlines = 120  " Minimum lines used for comment syncing (default 50)

" Rust settings
if filereadable("rustfmt.toml") || filereadable(".rustfmt.toml")
    let g:rustfmt_autosave = 1  " Format files on auto save if a rustfmt config exists
endif

" Snippets
let g:UltiSnipsSnippetDirectories=["UltiSnips"]

" Delimiter
let delimitMate_expand_cr = 1  " Put character on next line after pressing Enter

" vim-signature marker color based on vim-gitgutter or vim-signify
let g:SignatureMarkTextHLDynamic = 1

" Indentation markers
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" Visual settings
set cursorline  " Highlight current line
set number  " Enable line numbers.
set showtabline=2  " Always show tab bar.
set relativenumber  " Use relative line numbers. Current line is still in status bar.
set title  " Show the filename in the window titlebar.

" Do not wrap lines:
"set nowrap
"set textwidth=80  " Make it obvious where 80 characters is

" Or, wrap lines:
set wrap
set wrapmargin=0 textwidth=0
set linebreak
set showbreak=…
" highlight ColorColumn ctermbg=black guibg=black  " changes the column color

set noshowmode  " Don't show the current mode (airline.vim takes care of us)
set laststatus=2  " Always show status line
set colorcolumn=81 " Display a column after the 80th character
" Display multiple columns after the 80th character:
" execute "set colorcolumn=" . join(range(81,335), ',')

" Scrolling
set scrolloff=3  " Start scrolling three lines before horizontal border of window.
set sidescrolloff=3  " Start scrolling three columns before vertical border of window.

" comfortable-motion.vim
let g:comfortable_motion_scroll_down_key = "j"
let g:comfortable_motion_scroll_up_key = "k"

" Indentation
set autoindent  " Copy indent from last line when starting new line.
set shiftwidth=4  " The # of spaces for indenting.
set smarttab  " At start of line, <Tab> inserts shiftwidth spaces, <Bs> deletes shiftwidth spaces.
set softtabstop=4  " Tab key results in 2 spaces
set tabstop=4  " Tabs indent only 2 spaces
set expandtab  " Expand tabs to spaces

" Reformatting
set nojoinspaces  " Only insert single space after a '.', '?' and '!' with a join command.

" Toggle show tabs and trailing spaces (,c)
if has('win32')
  set listchars=tab:>\ ,trail:.,eol:$,nbsp:_,extends:>,precedes:<
else
  set listchars=tab:▸\ ,trail:·,eol:¬,nbsp:_,extends:>,precedes:<
endif
"set listchars=tab:>\ ,trail:.,eol:$,nbsp:_,extends:>,precedes:<
"set fillchars=fold:-

" Whitespace
set nolist  " Disable by default (Toggle using ToggleInvisibles function)

" Search / replace
set gdefault  " By default add g flag to search/replace. Add g to toggle.
set hlsearch  " Highlight searches
set incsearch  " Highlight dynamically as pattern is typed.
set ignorecase  " Ignore case of searches.
set smartcase  " Ignore 'ignorecase' if search pattern contains uppercase characters.

" Ignore things
set wildignore+=*.jpg,*.jpeg,*.gif,*.png,*.gif,*.psd,*.o,*.obj,*.min.js
set wildignore+=*/bower_components/*,*/node_modules/*
set wildignore+=*/vendor/*,*/.git/*,*/.hg/*,*/.svn/*,*/log/*,*/tmp/*

" Vim commands
set hidden  " When a buffer is brought to foreground, remember undo history and marks.
set report=0  " Show all changes.
set mouse=a  " Enable mouse in all modes.
set shortmess+=I  " Hide intro menu.

" Splits
set splitbelow  " New split goes below
set splitright  " New split goes right

" Use the system clipboard register by default
set clipboard=unnamedplus

" PLUGINS

" Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#tabline#buffer_nr_format = '%s '
let g:airline#extensions#tabline#buffer_nr_show = 1

" NERDTree
let NERDTreeShowHidden = 1
let NERDTreeMouseMode = 2
let NERDTreeMinimalUI = 1
let NERDTreeAutoDeleteBuffer = 1

" Signify
let g:signify_vcs_list = ['git', 'hg', 'svn']

" Indent Guides
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

" Mustache/handlebars
let g:mustache_abbreviations = 1

" " Enable auto-completion
" let g:deoplete#enable_at_startup = 1
" call deoplete#custom#option('smart_case', v:true)
set completeopt=longest,menuone,noinsert

" Cross-session undo
if has('persistent_undo')
  let g:undotree_SetFocusWhenToggle = 1
  set undofile
  set undodir=~/.vim/undodir
  set undolevels=1000
  set undoreload=10000
endif

" Backup directory
set backupdir=$DOTFILES/caches/vim

" Disable concealing
let g:vim_json_syntax_conceal = 0
