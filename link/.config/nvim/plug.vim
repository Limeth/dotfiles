" https://github.com/junegunn/vim-plug
" Reload .vimrc and :PlugInstall to install plugins.
call plug#begin('~/.vim/plugged')

" Utilities
Plug 'nanotech/jellybeans.vim' " color scheme
Plug 'vim-airline/vim-airline' " theme
Plug 'vim-airline/vim-airline-themes' " theme
Plug 'tpope/vim-sensible' " defaults
Plug 'tpope/vim-surround' " csi'
Plug 'tpope/vim-fugitive' " git wrapper
Plug 'tpope/vim-repeat' " better .
Plug 'tpope/vim-commentary' " commenting out code
Plug 'tpope/vim-unimpaired' " complementary [] mappings
Plug 'tpope/vim-eunuch' " SudoWrite and other unix tools
Plug 'scrooloose/nerdtree' " directory tree
Plug 'Xuyuanp/nerdtree-git-plugin' " git support for nerdtree
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fuzzy finder
Plug 'junegunn/fzf.vim' " fuzzy finder
Plug 'nathanaelkane/vim-indent-guides' " indentation visualization
Plug 'mhinz/vim-signify' " show affected lines since last commit in line number columns
Plug 'terryma/vim-multiple-cursors' " multiple cursors
Plug 'mbbill/undotree' " browsable undo history
Plug 'Konfekt/FastFold' " optimized folding
" Plug 'easymotion/vim-easymotion'
" Plug 'Shougo/neosnippet.vim'
" Plug 'Shougo/neosnippet-snippets'
" Plug 'Shougo/denite.nvim'
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Raimondi/delimitMate' " automating closing of [] () {} <> pairs
Plug 'christoomey/vim-tmux-navigator' " navigate between vim and tmux panes seemlessly
" Plug 'scrooloose/syntastic' " syntax linting incompatible with LSP
" Plug 'kshenoy/vim-signature' " mark-related commands and display
Plug 'vim-scripts/TaskList.vim' " <leader>t to display a list of TODO, FIXME, XXX
Plug 'vim-scripts/undofile_warn.vim' " warn when using the undofile
Plug 'haya14busa/incsearch.vim' " display all search matches
Plug 'haya14busa/incsearch-fuzzy.vim' " fuzzy search
Plug 'haya14busa/incsearch-easymotion.vim' " easy motion integration for incsearch
Plug 'yuttie/comfortable-motion.vim' " 'smooth' scrolling
Plug 'igankevich/mesonic' " meson build system support
Plug 'junegunn/vim-peekaboo' " show contents of buffers
Plug 'gioele/vim-autoswap' " Please Vim, stop with these swap file messages. Just switch to the correct window!
Plug 'tommcdo/vim-lion' " align text by character, e.g.: gl=
Plug 'puremourning/vimspector' " debugging

" Languages
Plug 'lervag/vimtex'
Plug 'fatih/vim-go', {'for': 'go'}
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
" Might want to fall back to master once the annoying error that appears
" whenever I save the file is fixed.
Plug 'klen/python-mode', {'for': 'python', 'branch': 'develop'}
Plug 'dag/vim-fish', {'for': 'fish'}
Plug 'rust-lang/rust.vim', {'for': 'rust'}
Plug 'cespare/vim-toml'
Plug 'mustache/vim-mustache-handlebars'
Plug 'chase/vim-ansible-yaml'
Plug 'wavded/vim-stylus'
Plug 'elzr/vim-json'
Plug 'ap/vim-css-color'
Plug 'tikhomirov/vim-glsl'
Plug 'Yggdroot/indentLine'
" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'Shougo/echodoc.vim'
Plug 'tomlion/vim-solidity'
Plug 'petRUShka/vim-opencl'
Plug 'luochen1990/rainbow'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'mattn/emmet-vim' " html macros
Plug 'wlangstroth/vim-racket' " racket (scheme) support
Plug 'ron-rs/ron.vim'
Plug 'sirtaj/vim-openscad'

call plug#end()
