""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" KEY BINDINGS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Character alignment
" `gl` will add spaces to the left of the alignment character, and `gL` will add spaces to the right.

" Do not close vim when C-z is pressed
nnoremap <C-z> <nop>

" Scrolling with comfortable-motion.vim
" Make it proportional to the window size
let g:comfortable_motion_no_default_key_mappings = 1
let g:comfortable_motion_speed = 4
let g:comfortable_motion_impulse_multiplier = 1 * g:comfortable_motion_speed
nnoremap <silent> <C-d> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 2)<CR>
nnoremap <silent> <C-u> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -2)<CR>
nnoremap <silent> <C-f> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 4)<CR>
nnoremap <silent> <C-b> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -4)<CR>
let g:comfortable_motion_interval = 1000.0 / 60.0  " TODO: Increase this once I get a 120 FPS monitor
let g:comfortable_motion_friction = 80.0 * g:comfortable_motion_speed
let g:comfortable_motion_air_drag = 2.0 * g:comfortable_motion_speed

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Set space to be the leader
map <space> <leader>
map <space><space> <leader><leader>

" remap redo
noremap U <C-r>

" Cross-session undo
if has('persistent_undo')
  " Warn about using the undofile
  noremap u     <Plug>(undofile-warn-undo)
  noremap <C-r> <Plug>(undofile-warn-redo)
endif

" One-way repeated search
nnoremap <expr> n 'Nn'[v:searchforward]
nnoremap <expr> N 'nN'[v:searchforward]

" Better up/down navigation
cnoremap <c-n> <down>
cnoremap <c-p> <up>

" Better screen re-draw
nnoremap <leader>l :nohlsearch<cr>:diffupdate<cr>:syntax sync fromstart<cr><c-l>

" Don't lose selection when shifting indent
xnoremap < <gv
xnoremap > >gv

" <Leader>c{char} to move to {char}
"
" The following line supposedly causes conflicts, but it makes <Leader><Leader>
" available for remapping.
map <Leader> <Plug>(easymotion-prefix)
map  <Leader>c <Plug>(easymotion-bd-f)
nmap <Leader>c <Plug>(easymotion-overwin-f)

" FZF selection mappings
nnoremap <leader><tab> <plug>(fzf-maps-n)
xnoremap <leader><tab> <plug>(fzf-maps-x)
onoremap <leader><tab> <plug>(fzf-maps-o)
" Insert mode completion
inoremap <c-x><c-k> <plug>(fzf-complete-word)
inoremap <c-x><c-f> <plug>(fzf-complete-path)
inoremap <c-x><c-j> <plug>(fzf-complete-file-ag)
inoremap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
" Overrides word completion command above
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})

"
" End of FZF mappings
"

" Source .vimrc & .gvimrc files
nnoremap <F12> :call SourceConfigs()<CR>
" Fix page up and down
map <PageUp> <C-U>
map <PageDown> <C-D>
imap <PageUp> <C-O><C-U>
imap <PageDown> <C-O><C-D>
" Move more naturally up/down when wrapping is enabled.
" Map jkl, to hjkl
noremap h <Nop>
noremap <C-h> <Nop>
noremap j h
noremap k gj
noremap l gk
noremap , l

" Remap the Join key to K, because I associated it with the down direction
noremap K J
noremap gK gJ

" Pane selection
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <C-j> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-k> :TmuxNavigateDown<cr>
nnoremap <silent> <C-l> :TmuxNavigateUp<cr>
nnoremap <silent> <C-,> :TmuxNavigateRight<cr>
nmap <silent> <C-\> <C-,>
" Tmux-like pane resizing
nnoremap <M-j> :call TmuxResize('left', 1)<CR>
nnoremap <M-k> :call TmuxResize('down', 1)<CR>
nnoremap <M-l> :call TmuxResize('up', 1)<CR>
nnoremap <M-,> :call TmuxResize('right', 1)<CR>
nnoremap <C-M-j> :call TmuxResize('left', 10)<CR>
nnoremap <C-M-k> :call TmuxResize('down', 10)<CR>
nnoremap <C-M-l> :call TmuxResize('up', 10)<CR>
nnoremap <C-M-,> :call TmuxResize('right', 10)<CR>
nmap <C-M-\> <C-M-,>

"
" Custom [Space]-[X] mappings
"

" Fuzzy-search files
nnoremap <leader>f :Files<CR>
" Fuzzy-search buffers
nnoremap <leader>b :Buffers<CR>
" Close the current buffer without closing the window or writing
nnoremap <leader>B :bd!<CR>
" Next buffer
nnoremap <leader>n :bnext<CR>
nnoremap <leader>N :bprev<CR>
" Open directory browser
nnoremap <leader>d :NERDTreeToggle<CR>
" Undo tree
nnoremap <leader>u :UndotreeToggle<CR>
" Jump to buffer number 1-99 with <leader><N>b
let c = 1
while c <= 99
  execute "nnoremap <silent> <leader>" . c . "b :" . c . "b<CR>"
  let c += 1
endwhile
" Trim extra whitespace
noremap <leader>ss :call StripExtraWhiteSpace()<CR>
" Clear last search
noremap <silent> <leader>? <Esc>:nohlsearch<CR>
" Toggle whitespace character visibility
nnoremap <silent> <leader>v :call ToggleInvisibles()<CR>
" Toggle fold
nnoremap <leader><leader> za

"
" Disable noob keys
"

" Disable arrow keys in Escape mode
nnoremap <buffer> <Up> <Esc>:echo 'Arrow keys? Seriously?'<CR>
nnoremap <buffer> <Down> <Esc>:echo 'Arrow keys? Seriously?'<CR>
nnoremap <buffer> <Left> <Esc>:echo 'Arrow keys? Seriously?'<CR>
nnoremap <buffer> <Right> <Esc>:echo 'Arrow keys? Seriously?'<CR>

" Disable arrow keys in Insert mode
inoremap <buffer> <Up> <Esc>:echo 'Arrow keys? Seriously?'<CR>
inoremap <buffer> <Down> <Esc>:echo 'Arrow keys? Seriously?'<CR>
inoremap <buffer> <Left> <Esc>:echo 'Arrow keys? Seriously?'<CR>
inoremap <buffer> <Right> <Esc>:echo 'Arrow keys? Seriously?'<CR>

" Disable arrow keys in Visual mode
vnoremap <buffer> <Up> <Esc>:echo 'Arrow keys? Seriously?'<CR>
vnoremap <buffer> <Down> <Esc>:echo 'Arrow keys? Seriously?'<CR>
vnoremap <buffer> <Left> <Esc>:echo 'Arrow keys? Seriously?'<CR>
vnoremap <buffer> <Right> <Esc>:echo 'Arrow keys? Seriously?'<CR>

"
" Language Server Protocol
"

" nnoremap <silent> <C-Space>  :call LanguageClient#textDocument_hover()<CR>
" nnoremap <silent> <Leader>h  :call LanguageClient#textDocument_documentHighlight()<CR>
" nnoremap <silent> <Leader>s  :call LanguageClient#textDocument_documentSymbol()<CR>
" nnoremap <silent> <Leader>S  :call LanguageClient#workspace_symbol()<CR>
" nnoremap <silent> <Leader>r  :call LanguageClient#textDocument_rename()<CR>
" nnoremap <silent> <Leader>e  :call LanguageClient#explainErrorAtPoint()<CR>
" nnoremap <silent> gtd        :call LanguageClient#textDocument_typeDefinition()<CR>
" nnoremap <silent> gd         :call LanguageClient#textDocument_definition()<CR>
" nnoremap <silent> gi         :call LanguageClient#textDocument_implementation()<CR>
" nnoremap <silent> gr         :call LanguageClient#textDocument_references()<CR>
" nnoremap <silent> gq         :call LanguageClient#textDocument_formatting()<CR>
" nnoremap <silent> <M-Return> :call LanguageClient#textDocument_codeAction()<CR>
" vnoremap <silent> <M-Return> :call LanguageClient#textDocument_codeAction()<CR>
" inoremap <silent> <M-Return> <Esc>:call LanguageClient#textDocument_codeAction()<CR>

command -nargs=0 RefactorFilewise :call LanguageClient#textDocument_rename()
command -nargs=0 MappingsLSP :echo "# Language Server Protocol Mappings
            \ \n
            \ \n\<C-Space\>  textDocument_hover
            \ \n\<Leader\>h  textDocument_documentHighlight
            \ \n\<Leader\>s  textDocument_documentSymbol
            \ \n\<Leader\>S  workspace_symbol
            \ \n\<Leader\>r  textDocument_rename
            \ \n\<Leader\>e  explainErrorAtPoint
            \ \ngtd        textDocument_typeDefinition
            \ \ngd         textDocument_definition
            \ \ngi         textDocument_implementation
            \ \ngr         textDocument_references
            \ \ngq         textDocument_rangeFormatting_sync
            \ \ngq         textDocument_formatting
            \ \n\<M-Return\> textDocument_codeAction"

"
" Auto-completion
"

inoremap <C-Space> <C-x><C-o>
noremap <C-@> <C-Space>

"
" Indentation width change
"
command -nargs=1 Indentation :set tabstop=<args> softtabstop=<args> expandtab shiftwidth=<args> smarttab
command -nargs=0 Tabs set autoindent noexpandtab tabstop=4 shiftwidth=4

"
" Improved incremental search
"
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
" Fuzzy search
map z/ <Plug>(incsearch-fuzzy-/)
map z? <Plug>(incsearch-fuzzy-?)
map zg/ <Plug>(incsearch-fuzzy-stay)
" easymotion integration
map <Leader>/ <Plug>(incsearch-easymotion-/)
map <Leader>? <Plug>(incsearch-easymotion-?)
map <Leader>g/ <Plug>(incsearch-easymotion-stay)
" easymotion + incsearch + fuzzy
noremap <silent><expr> <Leader>z/ incsearch#go(ConfigEasyFuzzyMotion())
