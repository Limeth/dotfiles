# Source dotfiles scripts
source ~/.dotfiles/bin/source.fish

# Enable terminal colors
set -x TERM "xterm-256color"

# Vi mode
# Automatically enable Vi mode in `~/.config/fish/functions/fish_user_key_bindings.fish`
set -g theme_display_vi yes
