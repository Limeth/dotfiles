function alfresco-mount -d "mount alfresco to /media/$USER/alfresco"
  sudo true
  set -l CVUT_PASSWORD (mpw -0 fel.cvut.cz)
  sudo mkdir -p "/media/$USER/alfresco"
  sudo mount -t cifs -o username="$CVUT_USERNAME",password="$CVUT_PASSWORD" //alfresco.feld.cvut.cz/alfresco "/media/$USER/alfresco"
end
