function cl -d "change directory and list files"
    cd $argv; ls -aC --group-directories-first
end
