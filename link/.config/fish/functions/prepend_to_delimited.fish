function prepend_to_delimited
  set ARGV_COUNT (count $argv)

  if [ $ARGV_COUNT -lt 3 ]
    echo "Not enough arguments for `prepend_to_delimited` specified."
    exit 1
  end

  set VARIABLE $argv[1]
  set DELIMITER $argv[2]
  set VALUE "$argv[3..$ARGV_COUNT]"

  if test "$$VARIABLE" = ""
    set -gx $VARIABLE "$VALUE"
  else
    set -gx $VARIABLE "$VALUE$DELIMITER$$VARIABLE"
  end

  true
end
