function fish_user_key_bindings
    fish_vi_key_bindings

    for mode in insert default visual
        bind -M $mode \cf forward-char
    end

    for mode in default normal visual
        bind --erase -M $mode h
        bind -M $mode j backward-char
        bind -M $mode k down-line
        bind -M $mode l up-line
        bind -M $mode , forward-char
    end
end


fzf_key_bindings
