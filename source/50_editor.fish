#!/usr/bin/env fish
# Editing

set -gx EDITOR 'nvim'
set -gx VISUAL "$EDITOR"
alias q "$EDITOR"
alias qv "q $DOTFILES/link/.{,g}vimrc +'cd $DOTFILES'"
alias qs "q $DOTFILES"
