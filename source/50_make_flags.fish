#!/usr/bin/env fish
# Neither the Vulkan SDK or Vulkan-capable drivers are installed automatically.
# Make sure to install them.

set -l THREADS (grep -c '^processor' /proc/cpuinfo)
set -gx MAKEFLAGS "-j $THREADS -l $THREADS"
