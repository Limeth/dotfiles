#!/usr/bin/env fish
set -gx SPECTRE_USERNAME 'Jakub Hlusička'

function mpw
    set -l PW (spectre -q $argv)
    echo $PW | xclip -rmlastnl
    echo $PW | xclip -rmlastnl -selection clipboard
end
