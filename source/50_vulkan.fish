#!/usr/bin/env fish
# Neither the Vulkan SDK or Vulkan-capable drivers are installed automatically.
# Make sure to install them.

if test -d ~/workspace/cpp/VulkanSDK
  set LATEST_EDITED (ls --color=never ~/workspace/cpp/VulkanSDK | sort -r | head -n 1)
  set -gx VULKAN_SDK ~/workspace/cpp/VulkanSDK/$LATEST_EDITED/x86_64
  set -gx PATH $VULKAN_SDK/bin $PATH
  prepend_to_delimited LD_LIBRARY_PATH : $VULKAN_SDK/lib
  set -gx VK_LAYER_PATH $VULKAN_SDK/etc/explicit_layer.d
end
