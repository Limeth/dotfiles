#!/usr/bin/env fish
# Most of this has already been automated, so the lines are commented out.
# RUST_PATH for personal reasons, others required
set -gx RUST_PROJECTS ~/workspace/rust
# set RUST_TOOLCHAIN (rustup toolchain list | grep default | sed "s/ (default)//")
# set -gx RUST_SRC_PATH ~/.multirust/toolchains/$RUST_TOOLCHAIN/lib/rustlib/src/rust/src
set -gx CARGO_HOME ~/.cargo
set -gx PATH $CARGO_HOME/bin $PATH

# Rust Language Server
# prepend_to_delimited LD_LIBRARY_PATH : (rustc --print sysroot)/lib
