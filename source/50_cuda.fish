#!/usr/bin/env fish
set -gx CUDAToolkit_ROOT /usr/local/cuda
set -gx PATH $CUDAToolkit_ROOT/bin $PATH
