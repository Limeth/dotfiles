alias grep "grep --color=auto"

# Prevent less from clearing the screen while still showing colors.
set -gx LESS -XR
