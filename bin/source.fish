#!/usr/bin/env fish
# Call this from your shell init script

# Where the magic happens.
set -gx DOTFILES ~/.dotfiles

# Add binaries into the path
set -gx PATH $DOTFILES/bin $PATH

function src -d 'Sources all files from directory `source`'
    if test -n "$argv[1]"
        source "$DOTFILES/source/$argv[1].sh"
    else
        # Reset the exit status to 0 after an unsuccessful `if` statement
        true
        for file in $DOTFILES/source/*.fish
            source "$file"
        end
    end
end

function dotfiles -d 'Run dotfiles script, then source'
    bash $DOTFILES/bin/dotfiles $argv; and src
end

src
