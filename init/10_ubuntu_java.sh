#!/usr/bin/env bash

sudo echo "Installing the Java Development Kit."

# Add webupd8team ppa for Oracle Java
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
sudo apt-get update -qq

# Automatically accept the Oracle license
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections

# Install Oracle JDK 8
sudo apt-get install oracle-java8-installer -y -qq

# Configure Java 8
sudo apt-get install oracle-java8-set-default -y -qq

# Install SDKMAN!, a distribution thingy for gradle
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

# Install gradle itself using SDKMAN!
sdk install gradle 4.1
