#!/usr/bin/env bash
fish -c '
set -l ORIGINAL_PWD (pwd)
set -l RUST_WORKSPACE ~/workspace/rust
set -l REPOSITORY_DIR "$RUST_WORKSPACE/alacritty"

# Install prerequisites
sudo apt-get install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3

# Get the repository
if test -d "$REPOSITORY_DIR"
    echo "Alacritty already installed, updating."
    cd "$REPOSITORY_DIR"
    git pull
else
    echo "Installing Alacritty."

    if not test -d "$REPOSITORY_DIR/.."
        mkdir -p "$REPOSITORY_DIR/.."
    end

    git clone --depth=1 https://github.com/jwilm/alacritty "$REPOSITORY_DIR"
    cd "$REPOSITORY_DIR"
end

rustup update stable
cargo +stable install --force --path=alacritty

# Terminfo
sudo tic -xe alacritty,alacritty-direct extra/alacritty.info

# Desktop Entry
sudo cp target/release/alacritty /usr/local/bin # or anywhere else in $PATH
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
sudo desktop-file-install extra/linux/Alacritty.desktop
sudo update-desktop-database

# Manual Page
sudo mkdir -p /usr/local/share/man/man1
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null

# Fish Completions
mkdir -p $fish_complete_path[1]
cp extra/completions/alacritty.fish $fish_complete_path[1]/alacritty.fish

cd "$ORIGINAL_PWD"

# Open Alacritty when the desktop shortcut for the terminal is pressed
gsettings set org.gnome.desktop.default-applications.terminal exec-arg ""
gsettings set org.gnome.desktop.default-applications.terminal exec "/home/limeth/.cargo/bin/alacritty"
'

