#!/usr/bin/env bash
# Install Fisher
fish -c 'curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher'

# Install plugins specified in the fishfile
fish -c 'fisher install oh-my-fish/theme-bobthefish fisherman/done fisherman/getopts fisherman/jellyfish'

# Finally, make the background color Jellyfish-friendly
gsettings set io.elementary.terminal.settings background '#151515'
