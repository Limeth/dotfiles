#!/bin/bash
cd $DOTFILES/caches
git clone https://github.com/petervanderdoes/gitflow-avh
cd gitflow-avh
git submodule init && git submodule update
sudo make install
