# Add the repository
sudo apt-get install -y -qq software-properties-common
sudo add-apt-repository -y ppa:neovim-ppa/stable

# Install Neovim
sudo apt-get update
sudo apt-get install -y -qq neovim

# Prerequisites for the Python modules:
sudo apt-get install -y -qq python-dev python-pip python3-dev python3-pip

# Python integration
sudo pip2 install neovim
sudo pip3 install neovim

# Setup aliases
sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
sudo update-alternatives --config vi
sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
sudo update-alternatives --config vim
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
sudo update-alternatives --config editor

# Download the plugin manager
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Backwards compatibility
# https://neovim.io/doc/user/vim_diff.html
ln -s ~/.config/nvim/init.vim ~/.vimrc
ln -s ~/.config/nvim ~/.vim

# Create backup directory
mkdir -p $DOTFILES/caches/vim

# Download Vim plugins.
if [[ "$(type -P vim)" ]]; then
  vim +PlugUpgrade +PlugUpdate +qall
fi
