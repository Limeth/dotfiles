# Enable colors in output
git config --global color.ui true
# Show simple git log
git config --global format.pretty oneline
# Push only the selected branch
git config --global push.default simple
