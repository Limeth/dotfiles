#!/bin/bash
sudo echo "
deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial main
deb-src http://apt.llvm.org/xenial/ llvm-toolchain-xenial main
# 3.8
deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.8 main
deb-src http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.8 main
# 3.9
deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.9 main
deb-src http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.9 main
" | sudo tee /etc/apt/sources.list.d/llvm.list

wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y -qq clang-3.9 clang-3.9-doc libclang-common-3.9-dev libclang-3.9-dev libclang1-3.9 libclang1-3.9-dbg libllvm-3.9-ocaml-dev libllvm3.9 libllvm3.9-dbg lldb-3.9 llvm-3.9 llvm-3.9-dev llvm-3.9-doc llvm-3.9-examples llvm-3.9-runtime clang-format-3.9 python-clang-3.9 liblldb-3.9-dev liblldb-3.9-dbg
sudo apt-get install -y -qq python-lldb-3.9

# create standardized symlinks
rm -f /usr/include/llvm
sudo ln -s /usr/include/llvm-3.9/llvm/ /usr/include/llvm
rm -f /usr/include/llvm-c
sudo ln -s /usr/include/llvm-c-3.9/llvm-c/ /usr/include/llvm-c
