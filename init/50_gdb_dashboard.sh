#!/usr/bin/env bash

mkdir -p "$DOTFILES/caches"
pushd "$DOTFILES/caches"

rm -rf gdb-dashboard
git clone https://github.com/cyrus-and/gdb-dashboard
pip3 install pygments

popd
