#!/usr/bin/env bash
# https://github.com/rapid7/metasploit-framework/wiki/Nightly-Installers#linux-and-os-x-quick-installation
msfinstall="$DOTFILES/caches/msfinstall" 
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > "$msfinstall" && \
  chmod 755 "$msfinstall" && \
  "$msfinstall"
