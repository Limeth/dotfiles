#!/usr/bin/env bash
fish -c '
# Install Rust nightly via Rustup
if not which rustup
    echo "Installing Rustup"
    curl --proto "=https" --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y -v --default-toolchain nightly
else
    echo "Rustup already installed, updating."
    rustup update
end

rustup install stable
rustup default nightly

# Install the Rust Language Server
rustup component add rls rust-analysis rust-src --toolchain nightly
rustup component add rls rust-analysis rust-src --toolchain stable
'
