#!/usr/bin/env bash
sudo cp "$DOTFILES/conf/ubuntu/udev-map-function-keys.rules" '/etc/udev/rules.d/90-map-function-keys.rules'
sudo udevadm control --reload-rules
sudo udevadm trigger
