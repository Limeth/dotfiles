#!/bin/bash
# Install the Fish shell
sudo apt-add-repository -y ppa:fish-shell/release-3
sudo apt-get update
sudo apt-get install -y -qq fish

# Make Fish the default shell
chsh -s `which fish`

# Prevent the Pantheon Terminal from closing when pressing Control + D
gsettings set io.elementary.terminal.settings save-exited-tabs false
