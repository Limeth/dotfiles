#!/usr/bin/env bash
# Add the release PGP keys:
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

# Add the "release" channel to your APT sources:
echo "deb http://apt.syncthing.net/ syncthing release" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo add-apt-repository -y ppa:nilarimogard/webupd8

# Update and install syncthing:
sudo apt-get update -y -qq
sudo apt-get install -y -qq syncthing syncthing-gtk
