#!/usr/bin/env bash
sudo apt-get install python3-pip
# sudo pip3 install powerline-status
sudo pip3 install git+https://github.com/powerline/powerline

# Clones and install the powerline fonts
git clone https://github.com/powerline/fonts.git ~/.dotfiles/caches/powerline-fonts
cd "$DOTFILES/caches/powerline-fonts"
chmod +x ./install.sh
./install.sh
git clone https://github.com/gabrielelana/awesome-terminal-fonts.git ~/.dotfiles/caches/awesome-fonts
cd "$DOTFILES/caches/awesome-fonts"
mkdir -p ~/.fonts
cp ./build/* ~/.fonts
gsettings set io.elementary.terminal.settings font 'Droid Sans Mono for Powerline'
