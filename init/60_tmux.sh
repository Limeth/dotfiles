#!/usr/bin/env bash
# Install tmux
# Development:
sudo apt-get remove tmux
sudo apt-get install -y -qq automake libevent-dev libncurses5-dev xclip bison
cd "$DOTFILES/caches"
rm -rf tmux
git clone https://github.com/tmux/tmux.git
cd tmux
sh autogen.sh
./configure && make && sudo make install
# Outdated:
# sudo apt-get install tmux

# Install project manager
# Disabled, using tmux-resurrect instead
# sudo gem install tmuxinator

# Install the Tmux Plugin Manager
rm -rf ~/.tmux/plugins/tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Run automatically
whoami=`whoami`
gsettings set io.elementary.terminal.settings shell "/home/$whoami/.dotfiles/conf/ubuntu/terminal-startup"
gsettings set io.elementary.terminal.settings tab-bar-behavior 'Hide When Single Tab'
