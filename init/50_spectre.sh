#!/usr/bin/env bash
fish -c '
sudo true
set -l ORIGINAL_PWD (pwd)
set -l REPOSITORY_DIR "$DOTFILES/caches/spectre-cli"
set -l PLATFORM_INDEPENDENT_CLI_C "$REPOSITORY_DIR"
set -l PLATFORM_INDEPENDENT_CLI_C_RESULT "/usr/local/bin/spectre"

# Install dependencies
sudo apt install -y -qq libsodium-dev libjson-c-dev

# Get the repository
if test -d "$REPOSITORY_DIR"
    echo "MasterPassword already installed, updating."
    cd "$REPOSITORY_DIR"
    git pull
else
    echo "Installing MasterPassword."

    if not test -d "$REPOSITORY_DIR/.."
        mkdir -p "$REPOSITORY_DIR/.."
    end

    git clone --depth=1 --recurse-submodules https://gitlab.com/spectre.app/cli $REPOSITORY_DIR
end

# Build it
cd "$PLATFORM_INDEPENDENT_CLI_C"
test -f ./clean; and ./clean
./build

# Install it
if test -f "$PLATFORM_INDEPENDENT_CLI_C_RESULT"
    sudo rm "$PLATFORM_INDEPENDENT_CLI_C_RESULT"
end

sudo install --mode=555 spectre "$PLATFORM_INDEPENDENT_CLI_C_RESULT"

cd "$ORIGINAL_PWD"
'
