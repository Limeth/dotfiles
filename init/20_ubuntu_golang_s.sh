#!/bin/bash
mkdir ~/workspace/golang
source $DOTFILES/source/50_golang.fish
go get -v github.com/zquestz/s
cd $GOPATH/src/github.com/zquestz/s
make
make install
