#!/usr/bin/env bash

# only needed for debugging
#exec >/tmp/udev.out 2>&1

HOME=/home/limeth
XAUTHORITY=$HOME/.Xauthority
export XAUTHORITY HOME
DISPLAY=:0 ; export DISPLAY;

# Path to lock file
lock="/tmp/keyboard.lock"

# Lock the file (other atomic alternatives would be "ln" or "mkdir")
exec 9>"$lock"
if ! flock -n 9; then
        notify-send -t 5000 "Keyboard script is already running."
        exit 1
fi

/bin/su limeth -c "sleep 2; xmodmap $HOME/.dotfiles/conf/ubuntu/xmodmap-map-function-keys.txt;" &

# The lock file will be unlocked when the script ends
echo '' > /tmp/keyboard.lock &
unset DISPLAY
